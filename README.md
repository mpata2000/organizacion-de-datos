# Organizacion de Datos

- [Organizacion de Datos](#organizacion-de-datos)
  - [**Introduccion**](#introduccion)
    - [**Que es un Data Scientist?**](#que-es-un-data-scientist)
      - [**Data Scientiest != Data Engineer**](#data-scientiest--data-engineer)
    - [**Machine Learning**](#machine-learning)
  - [**Visualizacion de datos**](#visualizacion-de-datos)
    - [**Datasaurus**](#datasaurus)
    - [**Numeros Utiles**](#numeros-utiles)
  - [**Plots**](#plots)
    - [**Distribucion continua**](#distribucion-continua)
      - [**Histogramas**](#histogramas)
      - [**Gráficos de densidad**](#gráficos-de-densidad)
      - [**Box Plots**](#box-plots)
    - [**Distribucion discreta**](#distribucion-discreta)
      - [**Bar Plots**](#bar-plots)
      - [**Stacked Bar Plots**](#stacked-bar-plots)
      - [**Tree Map**](#tree-map)
    - [**De relacion**](#de-relacion)
      - [**Scatter Plots**](#scatter-plots)
      - [**Heat Maps**](#heat-maps)
    - [**Series de tiempo**](#series-de-tiempo)
      - [**Line Plots**](#line-plots)
  - [**Falacias Con los Datos**](#falacias-con-los-datos)
    - [**Paradoja de Simpson**](#paradoja-de-simpson)
      - [**A/B Testing**](#ab-testing)
    - [**Sesgo de supervivencia**](#sesgo-de-supervivencia)
  - [**Variables**](#variables)
  - [**Corelacion de Variables**](#corelacion-de-variables)
  - [**Varianza**](#varianza)
  - [**Covarianza**](#covarianza)
  - [**Correlacion de Pearson**](#correlacion-de-pearson)
    - [**Correlación de Pearson: desvio estandar**](#correlación-de-pearson-desvio-estandar)
  - [**Regresion**](#regresion)
    - [**Regresion Lineal**](#regresion-lineal)
      - [**Ecuación Normal**](#ecuación-normal)
      - [Error en regresion](#error-en-regresion)
  - [**Clasificacion**](#clasificacion)
    - [**Regresion Logistica**](#regresion-logistica)
  - [**Metodos de clusterizacion**](#metodos-de-clusterizacion)
    - [**K-Means**](#k-means)
      - [**Regla del codo**](#regla-del-codo)
      - [**Metodo de Silhouette**](#metodo-de-silhouette)
      - [S**ilhoutte Plot**](#silhoutte-plot)
      - [**Estadistica de Hopkins**](#estadistica-de-hopkins)
  - [**Entreneamiento**](#entreneamiento)
    - [**K folds**](#k-folds)
      - [**Ejemplo con 5 k folds**](#ejemplo-con-5-k-folds)
    - [**Conjunto Balanceado**](#conjunto-balanceado)
    - [**Overfitting**](#overfitting)
  - [**Metricas**](#metricas)
  - [**Procesamiento y transformacon de datos**](#procesamiento-y-transformacon-de-datos)
    - [**Proceso de KDD (Knowledge Discovery in Databases)**](#proceso-de-kdd-knowledge-discovery-in-databases)
    - [**Preprocesamiento y transformación de datos**](#preprocesamiento-y-transformación-de-datos)
    - [**Limpieza de datos**](#limpieza-de-datos)
      - [**Tipos de Datos Faltantes**](#tipos-de-datos-faltantes)
        - [**Missing completely at random (MCAR)**](#missing-completely-at-random-mcar)
        - [**Missing not at random (MNAR)**](#missing-not-at-random-mnar)
        - [**Missing at random (MAR)**](#missing-at-random-mar)
      - [Estrategias para tratar datos faltantes](#estrategias-para-tratar-datos-faltantes)
        - [**Eliminar registros o variables**](#eliminar-registros-o-variables)
        - [**Imputación de datos**](#imputación-de-datos)
          - [**MICE - Multiple Imputation by Chained Equations**](#mice---multiple-imputation-by-chained-equations)
    - [**Feature Engineering**](#feature-engineering)
      - [**Normalizacion**](#normalizacion)
        - [**Min-Max**](#min-max)
        - [**Normalizacion Z-Score**](#normalizacion-z-score)
        - [**Decimal Scaling**](#decimal-scaling)
      - [**Transformaciones para lograr normalidad**](#transformaciones-para-lograr-normalidad)
      - [**Discretización**](#discretización)
        - [**Discretización - Binning**](#discretización---binning)
      - [**Variables Dummies – One Hot Encoding**](#variables-dummies--one-hot-encoding)
  - [**Analisis de Outliers**](#analisis-de-outliers)
    - [**Tipo de Outliers**](#tipo-de-outliers)
    - [Deteccion de Outliers](#deteccion-de-outliers)
      - [**Metodos Univariados**](#metodos-univariados)
        - [**Z-Score**](#z-score)
        - [**Z-Score Modificado**](#z-score-modificado)
        - [**Análisis de Box-Plot**](#análisis-de-box-plot)
      - [**Metodos Multivariados**](#metodos-multivariados)
        - [**Distancia de Mahalanobis**](#distancia-de-mahalanobis)
        - [**Local Outlier Factor (LOF)**](#local-outlier-factor-lof)
        - [**Isolation Forest**](#isolation-forest)
  - [**Arboles**](#arboles)
    - [**ID3 (Iterative Dichotomiser 3)**](#id3-iterative-dichotomiser-3)
      - [**ID3-Entropia**](#id3-entropia)
      - [**ID3-Ganancia de Informacion**](#id3-ganancia-de-informacion)
    - [**Impureza de Gini**](#impureza-de-gini)
    - [**C4.5**](#c45)
      - [**C4.5-Campos numericos, rangos continuos**](#c45-campos-numericos-rangos-continuos)
      - [**C4.5-Datos Faltantes**](#c45-datos-faltantes)
      - [**C4.5-Poda**](#c45-poda)
    - [**Hiperparametros Arbol**](#hiperparametros-arbol)
    - [**Random Forest**](#random-forest)
  - [**K-Nearest Neighbors (KNN)**](#k-nearest-neighbors-knn)
    - [**KNN Hiperparametros**](#knn-hiperparametros)
    - [**KNN Regresion**](#knn-regresion)
  - [**Support Vector Machine (SVM)**](#support-vector-machine-svm)
    - [**Vectores de Soporte**](#vectores-de-soporte)
    - [**Kerneles SVM**](#kerneles-svm)
      - [**SVM con kernel LINEAL**](#svm-con-kernel-lineal)
      - [**SVM con kernel POLINOMICO**](#svm-con-kernel-polinomico)
      - [**SVM con kernel RADIAL**](#svm-con-kernel-radial)
  - [**Ensambles**](#ensambles)
    - [**Errores**](#errores)
      - [**Error irreducible**](#error-irreducible)
      - [**Bias (sesgo)**](#bias-sesgo)
      - [**Varianza**](#varianza-1)
      - [**Bias vs Varianza**](#bias-vs-varianza)
    - [**Bagging**](#bagging)
    - [**Boosting**](#boosting)
      - [**AdaBoost**](#adaboost)
      - [**Gradient Boosting**](#gradient-boosting)
      - [**XGBoost**](#xgboost)
  - [**Ensambles Hibridos**](#ensambles-hibridos)
    - [**Voting**](#voting)
    - [**Stacking**](#stacking)
    - [**Cascading**](#cascading)
  - [**Reducción de Dimensionalidad**](#reducción-de-dimensionalidad)
    - [**PCA**](#pca)
    - [**MDS - Multi-Dimensional Scaling**](#mds---multi-dimensional-scaling)
    - [**t-SNE**](#t-sne)
    - [**ISOMAP**](#isomap)
  - [**Redes Neuronales**](#redes-neuronales)
    - [**Perceptron Simple**](#perceptron-simple)
    - [**Preceptron Multicapa**](#preceptron-multicapa)
      - [**Backpropagation**](#backpropagation)
      - [**Redes SOM(Self Organizing Maps)**](#redes-somself-organizing-maps)
  - [Implementación de Redes Neuronales](#implementación-de-redes-neuronales)
    - [**Overfitting en Redes Neuronales**](#overfitting-en-redes-neuronales)
      - [**Metodos de regularización**](#metodos-de-regularización)
        - [**L1 y L2**](#l1-y-l2)
        - [**Dropout**](#dropout)
        - [**Early stopping**](#early-stopping)
        - [**Data augmentation**](#data-augmentation)
      - [**Optimizadores**](#optimizadores)
        - [**SGD**](#sgd)
        - [**Momentum**](#momentum)
        - [**Nesterov**](#nesterov)
        - [**Adagrad**](#adagrad)
        - [**RMSProp**](#rmsprop)
        - [Adam (Adaptive moment estimation)](#adam-adaptive-moment-estimation)
        - [AdaMax](#adamax)
        - [Nadam](#nadam)
        - [Adadelta](#adadelta)
    - [Número de Capas](#número-de-capas)
    - [Otros Hiperparámetros](#otros-hiperparámetros)

## **Introduccion**

### **Que es un Data Scientist?**

Un Data Scientist es una persona que se encarga de analizar datos para obtener informacion y asi poder tomar decisiones. Para esto, el Data Scientist debe tener conocimientos de estadistica, programacion y analisis de datos.

Es capaz:

- Obtener, interpretar, procesar y filtrar datos.
- Llegar a conclusiones a partir de los datos.
- Construir soluciones para los problemas que se quiere resolver.

#### **Data Scientiest != Data Engineer**

El ingeniero de datos se enfoca en la creacion de la infraestructura y arquitectura para la generacion, soporte y extraccion de datos (Big Data). El Data Scientist se enfoca en la interpretacion y analisis de los datos mediante la aplicacion de tecnicas estadisticas y de machine learning.

![Data Scientist vs Data Engineer](Images/dataScience.png)

### **Machine Learning**

> El aprendizaje automático es la ciencia (y el arte) de programar computadoras para que aprendan a partir de datos.
>
> -- <cite>Aurélien Géron, 2019</cite>

> Se dice que un programa de computadoras aprende de la experiencia E, respecto de una tarea T y una medida de rendimiento R, si su rendimiento en T, medido por R, mejora la experiencia E.
>
> -- <cite>(Tom Mitchell, 1997)</cite>

> El aprendizaje automático es el campo de estudio que da a las computadoras la capacidad de aprender sin ser programadas de manera explícita.
>
> -- <cite>Arthur Samuel, 1959</cite>

## **Visualizacion de datos**

**Porque Graficamos**

- Las técnicas de visualización de datos son muy importantes tanto para nuestro trabajo como para comunicarlo
- La cantidad de tipos de gráficos disponibles es enorme y es importante entenderlos y saber para qué es útil cada uno
- Entender de forma eficiente los datos
- Comunicar de forma concisa y clara
- Encontrar patrones/relaciones
- Tratar con conjuntos de datos **multidimensionales** con más de una variable o atributo comienza a causar problemas, ya que estamos restringidos a comunicar en dos dimensiones (a lo sumo 3).
- No toda la información importante se puede adivinar a través del análisis estadístico...

### **Datasaurus**

El Datasaurus es un conjunto de datos que comparten la misma distribución de probabilidad, pero que tienen diferentes propiedades estadísticas. Esto permite comparar diferentes tipos de gráficos y ver cuál es el más adecuado para cada caso.

- Todos estos graficos tienen la misma media y desviacion estandar.

### **Numeros Utiles**

- Media: Promedio de los valores
- Mediana: Valor que divide la mitad de los valores
- Cuartiles: Valores que dividen el conjunto de datos en 4 partes iguales
- Rango intercuartilico: Rango entre el primer y tercer cuartil

## **Plots**

- Distribucion continua
  - Histograma
  - Gráfico de densidad
- Distribucion discreta
  - Gráfico de barras
- De relacion
  - Scatter plot
- Series de tiempo
  - Line plot

### **Distribucion continua**

#### **Histogramas**

Un histograma es una herramienta de visualización que muestra la distribución de una variable numérica. Para construir un histograma, se divide el rango de valores de la variable en intervalos llamados "bins" y se cuenta la cantidad de valores que caen en cada bin. El eje horizontal representa los valores de la variable y el eje vertical representa la frecuencia o cantidad de observaciones en cada bin. Es importante empezar con una base numérica de 0 para evitar distorsiones en la interpretación de la frecuencia. El número de bins puede ajustarse según se requiera para obtener mayor o menor detalle en la distribución.

![Histograma](Images/distribucionContinua.jpg)

**Tipos de distribucion en un histograma:**

![Histograma](Images/histograma.png)

- **Bins**

    El numero de bins es un parametro que se puede modificar. Cuantos mas bins, mas detalle se puede ver en el histograma. Pero tambien puede ser que se pierda informacion. Tambien hay que eleir bien el rango de los bins.

![Histograma](Images/binSize1.png)
![Histograma](Images/binSize2.png)

Distintos tamaños de bins pueden dar lugar a diferentes interpretaciones de la distribución de los datos.

#### **Gráficos de densidad**

![Gráfico de densidad](Images/densidad.jpg)

Los gráficos de densidad son una herramienta de visualización que muestra la distribución de una variable numérica. A diferencia de los histogramas, que dividen la variable en intervalos y muestran la cantidad de valores que caen en cada intervalo, los gráficos de densidad calculan la densidad de valores para cada intervalo. Esto da como resultado una suavización de la distribución y permite identificar patrones y tendencias de una manera más clara y precisa. Es importante tener en cuenta que la suavización puede hacer que la distribución parezca más suave de lo que realmente es, lo que puede llevar a interpretaciones erróneas de los datos.

#### **Box Plots**

![Box Plot](Images/boxPlot.png)

Los box plots son una herramienta visual para representar la distribución de un conjunto de datos numéricos mediante la mediana, los cuartiles y los valores atípicos. La caja central representa el rango intercuartilico (IQR), que contiene el 50% de los datos. Las líneas que se extienden desde la caja muestran el rango completo de los datos, excluyendo los valores atípicos, que se muestran como puntos individuales. Los box plots son útiles para comparar la distribución de varias variables numéricas y para detectar valores atípicos que pueden afectar la interpretación de los datos.

- **Outliers**

    Los valores atípicos, o *outliers* en inglés, son valores que se encuentran muy lejos del resto de los valores en un conjunto de datos. En un box plot, se muestran como puntos individuales más allá de las líneas que se extienden desde la caja. Los outliers pueden ser el resultado de errores de medición, datos no representativos o simplemente valores extremos en el conjunto de datos. Es importante tener en cuenta los outliers al analizar los datos, ya que pueden afectar la interpretación de los resultados.

### **Distribucion discreta**

#### **Bar Plots**

![Bar Plot](Images/barPlot.png)

es una forma común de visualización de datos que utiliza barras rectangulares para mostrar la relación entre variables. En un bar plot, cada variable se representa por una barra y la altura de la barra representa la magnitud de la variable.

El eje horizontal del bar plot representa las diferentes categorías de la variable y el eje vertical representa la magnitud de la variable. Las barras se pueden colorear de acuerdo con una escala de colores para resaltar diferentes categorías o variables.

#### **Stacked Bar Plots**

![Stacked Bar Plot](Images/stackedBarPlot.jpg)

Un stacked bar plot (también conocido como diagrama de barras apiladas) es una variante del diagrama de barras en la que varias variables se apilan una encima de la otra para mostrar su contribución a una totalidad. Cada barra se divide en segmentos, y cada segmento representa la magnitud de una variable específica.

En un stacked bar plot, el eje horizontal representa las categorías de la variable y el eje vertical representa la magnitud de la variable. Los segmentos se pueden colorear de diferentes maneras para resaltar las diferentes variables y para mostrar cómo contribuyen a la totalidad.

Los stacked bar plots se utilizan comúnmente para representar datos que muestran cómo se divide una totalidad en diferentes partes, como para mostrar la composición de una población por edad o género, o para mostrar cómo se dividen los gastos de una organización por categoría. También se pueden utilizar para comparar diferentes conjuntos de datos, como para comparar las contribuciones de diferentes departamentos de una empresa a los ingresos totales.

#### **Tree Map**

![Tree Map](Images/treeMap.png)

Un tree map (también conocido como mapa de árbol o diagrama de áreas) es una visualización jerárquica de datos que utiliza rectángulos para representar la información. Cada rectángulo representa una categoría o subcategoría, y su tamaño es proporcional a la cantidad de datos que representa.

Los rectángulos se organizan en una estructura de árbol, donde los rectángulos más grandes se dividen en rectángulos más pequeños que representan subcategorías. Cada rectángulo puede estar coloreado de acuerdo con una escala de colores, para ayudar a visualizar los valores de los datos.

Los tree maps se utilizan comúnmente en la visualización de datos financieros, pero también pueden ser útiles para visualizar cualquier tipo de jerarquía de datos, como la estructura de un sitio web o la organización de una biblioteca.

### **De relacion**

#### **Scatter Plots**

![Scatter Plot](Images/scatterPlots.png)

Un scatter plot (también conocido como diagrama de dispersión) es una herramienta de visualización que muestra la relación entre dos variables numéricas. Cada punto en el scatter plot representa un valor de una variable en el eje horizontal y un valor de la otra variable en el eje vertical. Los puntos se pueden colorear de acuerdo con una escala de colores para resaltar diferentes categorías o variables.

#### **Heat Maps**

![Heat Map](Images/heatMap.png)

Un heat map (también conocido como mapa de calor) es una herramienta de visualización que muestra la relación entre dos variables numéricas. Cada celda en el heat map representa un valor de una variable en el eje horizontal y un valor de la otra variable en el eje vertical. Las celdas se pueden colorear de acuerdo con una escala de colores para resaltar diferentes categorías o variables.

### **Series de tiempo**

#### **Line Plots**

![Line Plot](Images/linePlot.png)

Un line plot (también conocido como gráfico de línea) es una herramienta de visualización que muestra la relación entre dos variables numéricas. Cada punto en el line plot representa un valor de una variable en el eje horizontal y un valor de la otra variable en el eje vertical. Los puntos se pueden conectar con líneas para mostrar la tendencia de los datos.

## **Falacias Con los Datos**

### **Paradoja de Simpson**

La paradoja de Simpson es un fenómeno estadístico que ocurre cuando una tendencia aparece en diferentes grupos de datos, pero desaparece o se invierte cuando los grupos se combinan. En otras palabras, puede ocurrir que una relación que parece ser verdadera en cada subgrupo de una población, sea falsa cuando se analiza la población en su conjunto.

La paradoja de Simpson se debe a que, a veces, la distribución de las variables en los subgrupos de una población puede ser diferente a la distribución en la población en su conjunto. Por lo tanto, es importante tener en cuenta esta paradoja y analizar los datos en detalle para evitar interpretaciones erróneas de las relaciones entre variables.

#### **A/B Testing**

El A/B testing (también conocido como prueba A/B o experimento A/B) es una técnica de prueba estadística utilizada en marketing digital y diseño web para comparar dos versiones de una página web, correo electrónico, anuncio o cualquier otra forma de contenido, con el fin de determinar cuál de las dos versiones es más efectiva para lograr un objetivo específico, como aumentar la tasa de conversión, el número de clics o la tasa de apertura.

### **Sesgo de supervivencia**

El sesgo de supervivencia es un sesgo estadístico que se produce cuando se basan los resultados de un estudio sólo en los objetos o individuos que han sobrevivido a un cierto evento o período de tiempo, ignorando a los que no lo han hecho. Esto puede llevar a conclusiones inexactas o engañosas, ya que los individuos o objetos que han sido eliminados del estudio pueden tener características diferentes a los que sobreviven.

## **Variables**

- Variables Independientes(entrada)
  - Cualitativas
    - Texto
      - Nominales
      - Ordinales
    - NUmericas
      - Nominales
      - Ordinales
  - Cuantitativas
    - Discretas
    - Continuas
- Variables Dependientes(salida,categoria)

1. Si la variable dependiente es cualitativa, el tipo de problema es de clasificación
2. Si la variable dependiente es cuantitativa, el problema es de regresión
3. Si NO hay variable dependientes, el problema es de agrupamiento

**Categóricas o cualitativas:** Las distintas modalidades que adoptan estas variables sólo se distinguen por ser diferentes, no se puede establecer un ordenamiento entre ellas. Son ejemplos de estas variables: color de cabello, tipo de auto, sexo.

**Cuasicuantitativas u ordinales:** En estas variables, si bien se puede ordenar las modalidades que adopta, no se puede establecer una distancia entre ellas. Por ejemplo: calificación de examen (A, B, C, D y E), estadío de una enfermedad (I, II, III o IV).

**Cuantitativas discretas:** Estas variables toman valores numéricos siendo que entre dos valores consecutivos de las mismas no existen valores intermedios. Pueden tomar un conjunto a lo sumo numerable de valores, vinculándose generalmente al proceso de contar. Son ejemplos de estas variables: cantidad de hijos, cantidad de materias aprobadas, dinero en una billetera.

**Cuantitativas continuas:** Estas variables también toman valores numéricos, pero entre dos valores de la variable existen infinitos valores intermedios, asociándose generalmente al proceso de medir. Son ejemplos de estas variables: peso, edad, duración de un llamado

## **Corelacion de Variables**

Dos variables están correlacionadas cuando varían de igual forma sistemáticamente. La correlación puede ser positiva o negativa. La correlación positiva ocurre cuando una variable aumenta mientras que la otra también lo hace. La correlación negativa ocurre cuando una variable aumenta mientras que la otra disminuye. Tambien puede no haber correlación entre las variables.

![](Images/Correlacion.png)

**La correlacion no implica casualidad:**

- Que dos variables tengan alto índice de correlación no significa que una cause la otra
- Las relaciones de causalidad son mucho más difíciles de encontrar y demostrar
- Las correlaciones pueden suceder por otros motivos como: Una tercer variable que “empuja” a ambas o simplemente azar

## **Varianza**

![Varianza](Images/varianza1.png)

La varianza es una medida de dispersión que indica qué tan lejos están los valores de una variable de su media. La varianza se calcula como la media de las diferencias al cuadrado entre cada valor y la media de la variable. La varianza se suele representar con la letra griega sigma (σ2).

![Varianza](Images/varianza2.png)

## **Covarianza**

![Covarianza](Images/covarianza.png)

En probabilidad y estadística, la covarianza es un valor que indica el grado de variación conjunta de dos variables aleatorias respecto a sus medias.

Es el dato básico para determinar si existe una dependencia entre ambas variables y además es el dato necesario para estimar otros parámetros básicos, como el coeficiente de correlación lineal o la recta de regresión.

**Covarianza Positiva**

![Covarianza](Images/covarianzaPositiva.png)

**Covarianza Negativa**

![Covarianza](Images/coviaranzaNegativa.png)

## **Correlacion de Pearson**

![Correlacion de Pearson](Images/correlacionDePearson.png)

La correlación de Pearson es un estadístico que mide la relación lineal entre dos variables numéricas. El valor de la correlación de Pearson se calcula como el cociente entre la covarianza de las dos variables y el producto de sus desviaciones estándar.

El valor de la correlación de Pearson se encuentra entre -1 y 1, donde indica:

- 1 indica una correlación positiva perfecta
- -1 indica una correlación negativa perfecta
- 0 indica que no hay correlación lineal

### **Correlación de Pearson: desvio estandar**

![Correlacion de Pearson](Images/correlacionDesvioEstandar.png)

Es una medida que se utiliza para cuantificar la variación o la dispersión de un conjunto de datos numéricos.
Una desviación estándar baja indica que la mayor parte de los datos de una muestra tienden a estar agrupados cerca de su media (también denominada el valor esperado), mientras que una desviación estándar alta indica que los datos se extienden sobre un rango de valores más amplio.

## **Regresion**

La regresión es un método estadístico que permite predecir el valor de una variable a partir de los valores de otras variables. La regresión se utiliza para predecir el valor de una variable dependiente (Y) a partir de los valores de una o más variables independientes (X).

### **Regresion Lineal**

![Regresion Lineal](Images/regresionLineal.png)

En la regresión lineal se trata de ajustar una línea recta a los datos para predecir y entender cómo varía la variable dependiente en función de los valores de las variables independientes.

En la regresión lineal, se busca encontrar los coeficientes de la línea recta que mejor se ajuste a los datos, minimizando la distancia entre la línea y los puntos de datos. Una vez que se ha ajustado el modelo, se puede utilizar para hacer predicciones sobre la variable dependiente en función de los valores de las variables independientes.

#### **Ecuación Normal**

```math
\beta = (X^{T}X)^{-1} X^{T}Y
```

Donde:

- β es el vector de coeficientes de la regresión lineal
- X es la matriz de diseño, que incluye todas las variables independientes
- Y es el vector de la variable dependiente

La ecuación normal calcula el inverso de X⊺ X, que es una matriz de  (n + 1) × (n + 1)
(donde n es el número de características).
La complejidad computacional de invertir tal matriz es típicamente alrededor de
O(n 2.4) a O(n3 ), dependiendo de la implementación.

#### Error en regresion

**Raíz del error cuadrático medio:**

![Error en regresion](Images/RMSE.png)

**Error medio absoluto:**

![Error en regresion](Images/MAE.png)

**Error cuadrático medio:**

![Error en regresion](Images/MSE.png)

**Para todas las formulas:**

- m: número de instancias
- h: función de hipótesis, es el modelo entrenado. En este caso regresión lineal
- x: todos los valores de entradas, todas las columnas

## **Clasificacion**

Cuando resolvemos un problema de clasificación, buscamos, para ciertos datos de entrada, un categoría c de un conjunto C de categorías posibles. Estas categorías no solo son finitas, sino que además son conocidas de antemano.

### **Regresion Logistica**

En la regresión logística lo que quiero es categorizar, clasificar. Es decir que dado una serie de puntos quiero encontrar una función que separe los puntos en dos, en dos conjuntos. Y una vez que la encontré puedo determinar para cualquier valor X futuro, el conjunto al cual pertenecerá. Está asociado a problemas de
probabilidad.

![Regresion Logistica](Images/regresionLogistica.png)

![Regresion Logistica](Images/formulaRegresionLogistica.png)

## **Metodos de clusterizacion**

En este tipo de problemas se trata de agrupar los datos de tal forma que queden definidos N conjuntos distinguibles, aunque no necesariamente se sepa que signifiquen esos conjuntos.

El agrupamiento siempre será por características similares.

### **K-Means**

![K-Means](Images/K-Means.png)

El algoritmo K-Means es un algoritmo de agrupamiento que agrupa los datos en K grupos. El algoritmo funciona de la siguiente manera:

1. El usuario decide la cantidad de grupos
2. K-Means elige al azar K centroides
3. Decide qué grupos están más cerca de cada centroide. Esos puntos forman un grupo
4. K-Means recalcula los centroides al centro de cada grupo
5. K-Means vuelve a reasignar los puntos usando los nuevos centroides. Calcula nuevos grupos
6. K-means repite punto 4 y 5 hasta que los puntos no cambian de grupo.

![K-Means](Images/kmean-steps.gif)

#### **Regla del codo**

![Regla del codo](Images/reglaDelCodo.png)

La regla del codo es una técnica para determinar el número óptimo de clusters en un algoritmo de agrupamiento. La regla del codo consiste en calcular la suma de los cuadrados de las distancias de cada punto a su centroide para cada valor de K. El valor de K que minimiza la suma de los cuadrados es el número óptimo de clusters.

#### **Metodo de Silhouette**

El coeficiente de silueta es una medida de cuán similar es un punto a su propio grupo (cohesión) en comparación con otros grupos (separación). El coeficiente de silueta varía entre -1 y 1, donde un valor de 1 indica que el punto está muy lejos de los otros grupos y muy cerca de su propio grupo, mientras que un valor de -1 indica que el punto está muy cerca de los otros grupos y muy lejos de su propio grupo.

![Metodo de Silhouette](Images/silhouette.png)

```math
s = \frac{b(i) - a(i)}{max(a(i), b(i))}
```

Donde:

- a(i) es la distancia media entre el punto i y todos los puntos en su propio grupo
- b(i) es la distancia media más pequeña entre el punto i y todos los puntos en otro grupo

#### S**ilhoutte Plot**

![Silhoutte Plot](Images/silhouttePlot1.png)

![Silhoutte Plot](Images/silhouttePlot2.png)

#### **Estadistica de Hopkins**

La estadística de Hopkins (Lawson y Jurs 1990) se utiliza para evaluar la tendencia de agrupación de un conjunto de datos midiendo la probabilidad de que un conjunto de datos dado sea generado por una distribución de datos uniforme.

En otras palabras, prueba la aleatoriedad espacial de los datos.

Sea D un conjunto de datos reales:

1. Tomar una muestra uniformemente de n puntos (p1,…, pn) de D.
2. Calcular la distancia, xi, de cada punto real a cada vecino más cercano.
   - Para cada punto pi ∈ D, encuentre su vecino más cercano pj; l
   - calcular la distancia entre pi y pj y llámela xi=dist(pi , pj)
3. Generar un conjunto de datos simulados (randomD) extraído de una distribución uniforme aleatoria con n puntos (q1,…, qn) y la misma variación que el conjunto de datos reales original D.
4. Calcular la distancia, yi desde cada punto artificial hasta el punto de datos real más cercano.
    - Para cada punto qi ∈ randomD, encuentre su vecino más cercano qj en D
    - calcular la distancia entre qi y qj y llámela yi=dist(qi , qj)
5. Calcule la estadística de Hopkins (H) como: la distancia media del vecino más cercano en el conjunto de datos aleatorios dividida por la suma de las distancias medias del vecino más cercano en el conjunto de datos real y simulado.

![Estadistica de Hopkins](Images/estadisticaDeHopkins.png)

Si D está distribuida de forma uniforme, entonces ∑ xi y ∑ yi   serían muy parecidos, entonces H sería aproximadamente ½ (0.5).

Pero si hay clústeres en D, las distancias de los puntos artificiales ∑ yi serían mucho más grandes que las distancias de los puntos reales:  ∑ xi y por lo tanto H sería mayor que 0.5.

Un valor de H superior a 0,75 indica una tendencia a la agrupación en un nivel de confianza del 90 %.

**Hipótesis que maneja Hopkins:**

- Hipótesis nula: el conjunto de datos D se distribuye uniformemente (es decir, no hay clusters significativos)
- Hipótesis alternativa: el conjunto de datos D no está uniformemente distribuido (es decir, contiene clusters significativos)

## **Entreneamiento**


El codigo para hacer el split de training , divide el conjunto de datos en dos partes, una para entrenamiento y otra para testeo en una proporcion de 70% y 30% respectivamente. El random state es para que siempre se divida de la misma forma.

`x_train, x_test, y_train, y_test = train_test_split(ds_trabajo_x, ds_trabajo_y, test_size=0.3, random_state=2) `

### **K folds**

El conjunto de entrenamiento se divide en $k$ conjuntos más pequeños. Para cada uno de los $k$ “folds” se sigue el siguiente procedimiento:
- Se entrena un modelo utilizando los $k$ “folds” como datos de entrenamiento.
- El modelo resultante se valida con la parte restante de los datos (el conjunto azul que le corresponde a la partición).
- La medida de rendimiento informada por la validación cruzada de $k$ veces es entonces el promedio de los valores calculados en el bucle o el mejor de todos.

![Entrenamiento](Images/conjuntoDeEntrenamiento1.png)

![Entrenamiento](Images/conjuntoDeEntrenamiento2.png)

#### **Ejemplo con 5 k folds**

Si tenemos 100 datos y hacemos el split de training nos quedan 70 datos para entrenar y 30 para testear. Si hacemos 5 k folds, entonces el conjunto de entrenamiento se divide en 5 conjuntos de 14 datos cada uno. Entonces se entranan con 4 folds y se valida con el restante. Esto se hace 5 veces, una vez por cada fold. Al final se promedian los resultados de cada fold o me quedo con el mejor.

### **Conjunto Balanceado**

![Conjunto Balanceado](Images/conjuntosBalanceados.png)

Un conjunto de datos balanceado es aquel en el que las clases están representadas en la misma proporción. Por ejemplo, si tenemos un conjunto de datos con 1000 instancias, y 500 de ellas pertenecen a la clase A y 500 a la clase B, entonces el conjunto de datos está balanceado.

### **Overfitting**

![Overfitting](Images/overfitting.png)

El overfitting es un problema que ocurre cuando el modelo se ajusta demasiado a los datos de entrenamiento. Esto significa que el modelo no generaliza bien a datos nuevos.

## **Metricas**

- **True Positive (TP):** Cuando se predice algo y se acierta.
- **False Positive (FP)**: Cuando se predice algo y no se acierta.
- **False Negative (FN):** Cuando se dice que algo no va a ocurrir y ocurre.
- **True Negative (TN):** Cuando se dice que algo no va a ocurrir y no ocurre.

```math

Precision = \frac{TP}{TP + FP} \\
\\
Recall = \frac{TP}{TP + FN} \\
\\
F1 = 2 * \frac{Precision * Recall}{Precision + Recall} \\
\\
Accuracy = \frac{TP + TN}{TP + TN + FP + FN}

```

---

## **Procesamiento y transformacon de datos**

### **Proceso de KDD (Knowledge Discovery in Databases)**

1. Entendimiento del dominio
2. Selección de datos
3. Procesamiento de datos
4. Transformación de datos
5. Data Mining
6. Interpretación y evaluación de resultados
7. Conocimiento descubierto

### **Preprocesamiento y transformación de datos**

Algunas tareas de estas etapa:

- Integracion de datos: unir datos de diferentes fuentes
- Limpieza de datos: completar valores faltantes, eliminar ruido, eliminar outliers, etc.
- Reducción de datos: reducir el tamaño del conjunto de datos
- Transformación de datos: transformar los datos para que sean más adecuados para el algoritmo de aprendizaje automático

Es lo que mas tiempo toma en el proceso de KDD

### **Limpieza de datos**

La limpieza de datos es el proceso de detectar y corregir (o eliminar) valores incorrectos de un conjunto de datos.

#### **Tipos de Datos Faltantes**

##### **Missing completely at random (MCAR)**

En este caso la razón de la falta de datos es ajena a los datos mismos.
No existen relaciones con la variable misma donde se encuentran los datos faltantes, o con las restantes variables en el dataset que expliquen porque faltan.

##### **Missing not at random (MNAR)**

La razón por la cual faltan los datos depende precisamente de los mismos datos que hemos recolectado (está relacionado con la razón por la que falta)

Ej: Si preguntas el nombre de la mascota pero en otra pregunta preguntas si tiene mascota, si la respuesta es no, entonces no se llenará el nombre de la mascota.

##### **Missing at random (MAR)**

Punto intermedio entre los dos anteriores.

La causa de los datos faltantes no depende de estos mismos datos faltantes, pero puede estar relacionada con otras variables del dataset.
Por ejemplo: encuestas mal diseñadas

#### Estrategias para tratar datos faltantes

##### **Eliminar registros o variables**

Si la eliminación de un subconjunto disminuye significativamente la utilidad de los datos, la eliminación del caso puede no ser efectiva
(No se recomienda en situaciones que no sean MCAR)

##### **Imputación de datos**

La imputación de datos es el proceso de reemplazar los datos faltantes con valores sustitutos.

- **Sustitucion de casos:** Se debe remplazar con valores no observados, deberia ser ealizado por un experto del dominio
- **Imputación de *Media* o *Mediadan*:** reemplazar los valores faltantes con la media/mediana de los valores no faltantes. Desventajas:
  - La varianza estimada de la nueva variable no es válida porque está atenuada por los valores repetidos
  - Se distorsiona la distribución
  - Las correlaciones que se observen estarán deprimidas debido a la repetición de un solo valor constante
- **Imputacion Cold Deck:** Selecciona valores o usa relaciones obtenidas de fuentes distintas de la base de datos actual. Ej: Localidad de un cliente en base a su codigo postal
- **Imputacion por regresion:** El dato faltante es reemplazado con el valor predicho por un modelo de regresión

###### **MICE - Multiple Imputation by Chained Equations**

Trabaja bajo el supuesto de que el origen de los faltantes es MAR

Es un proceso de imputación de datos faltantes iterativo, en el cual, en cada iteración cada valor faltante de cada variable se predice en función de las variables restantes.

Esta iteración se repite hasta que se encuentre convergencia en los valores.Por lo general 10 iteraciones es suficiente. (En cada iteración genera un dataset)

![MICE](Images/MICE.png)

### **Feature Engineering**

Es el proceso de usar el conocimiento del dominio para extraer características de los datos que hagan que los algoritmos de aprendizaje automático funcionen mejor.

#### **Normalizacion**

Se aplica sobre valores numéricos, consiste en escalar los features de manera que puedan ser mapeados a un rango más pequeño.

Es pricipamente usado cuando:

- Las unidades de medidas dificultan la comparación de features
- Se quiere evitar que atributos con mayores magnitudes tengan pesos muy diferentes al resto

![Normalizacion](Images/normalizacion-1.png)

![Normalizacion](Images/normalizacion-2.png)

##### **Min-Max**

![Min-Max](Images/min-max.png)

Funciona al ver cuánto más grande es el valor actual del valor mínimo del
feature y escala esta diferencia por el rango. Los valores resultantes se encuentran entre 0 y 1.

##### **Normalizacion Z-Score**

Los valores para un atributo se normalizan en base a su media y desvío
estándar

![Z-Score](Images/z-score.png)

Es util cuando el verdadero rango de valores es desconocido o puede variar o cuando se sospecha que los datos pueden tener outliers.

##### **Decimal Scaling**

Asegura que cada valor normalizado se encuentra entre - 1 y 1.

![Decimal Scaling](Images/decimal-scaling.png)

Donde d representa el número de dígitos en los valores de la variable con el valor absoluto más grande

#### **Transformaciones para lograr normalidad**

Podemos reducir este sesgo a partir de transformaciones, por ejemplo:

- Raíz cuadrada
- Logaritmos
- Inversa de la raíz cuadrada
- Transformaciones de Box-Cox

#### **Discretización**

Es una técnica que permite dividir el rango de una variable continua en intervalos.

Se reducen los valores de una variable contínua a un número reducido de etiquetas

##### **Discretización - Binning**

Se divide a la variable en un número específico de bins

Los criterios de agrupamiento pueden ser por ejemplo:

- Igual-Frecuencia: La misma cantidad de observaciones en un bin
- Igual-Ancho: Definimos rangos o intervalos de clases para cada bin
- Cuantiles: Separar en intervalos utilizando Mediana, Cuantiles, Percentiles.

A su vez para cada uno de los agrupamientos podemos hacer:

- Reemplazo por media o mediana
- Reemplazo por una etiqueta o valor entero

![Binning](Images/D-binning.png)

#### **Variables Dummies – One Hot Encoding**

Algunos métodos analíticos requieren que las variables predictoras sean numéricas

Cuando tenemos categóricos, podemos recodificar la variable categórica en una o más variables Dummies

![Dummies](Images/dummies.png)

---

## **Analisis de Outliers**

Un outlier es una observación que se encuentra muy lejos de la mayoría de las otras observaciones.

Es un concepto subjetivo al problema.

- Son observaciones distantes del resto de los datos
- Pueden deberse a un error de medición, aleatoriedad, que esa instancia pertenezca a una familia distinta del resto, etc

La detección de outliers es importante su presencia puede influenciar los resultados de un análisis estadístico clásico.

**¿Es necesario eliminarlos?**
- Deben ser cuidadosamente inspeccionados
- Pueden estar alertando anomalías, en algunas situaciones nuestra tarea de interés será encontrarlos :
  - Detección de Fraudes
  - Detección de Fallas
  - Patologías Médicas

### **Tipo de Outliers**

![Outliers](Images/tipo-outlier.png)

**Univariado** 
  - Son valores atípicos que podemos encontrar en una simple variable.
  - El problema de los enfoques univariados es que son buenos para detección de extremos pero no en otros casos.

**Multivariado** 
  - Los valores atípicos multivariados se pueden encontrar en un espacio n-dimensional.
  - Para detectar valores atípicos en espacios n-dimensionales es necesario ajustar un modelo.

### Deteccion de Outliers

En grandes volúmenes de datos la detección de outliers resulta más eficiente estudiando todas las variables.

Los outliers, en casos multivariados, pueden provocar dos tipos de efectos:
- El **efecto de enmascaramiento** se produce cuando un grupo de outliers esconden a otro/s. Es decir, los outliers enmascarados se harán visibles cuando se elimine/n el o los outliers que los esconden.
- El **efecto de inundación** ocurre cuando una observación sólo es outlier en presencia de otra/s observación/es. Si se quitara/n la/s última/s, la primera dejaría de ser outlier.

#### **Metodos Univariados**

##### **Z-Score**

Z-Score es una métrica que indica cuántas desviaciones estándar tiene una observación de la media muestral, asumiendo una distribución gaussiana.

![Z-Score](Images/z-score.png)

Cuando calculamos Z-Score para cada muestra debemos fijar un umbral:

- Un valor como “regla de oro” es Z > 3

##### **Z-Score Modificado**

La media de la muestra y la desviación estándar de la muestra, pueden verse afectados por los valores extremos presentes en los datos

![Z-Score Modificado](Images/z-score-mod.png)

Es la mediana de los desvíos absolutos respecto de la mediana.

Regla de oro valores mayores a 3.5 son considerados outliers

##### **Análisis de Box-Plot**

Los Box-Plots permiten visualizar valores extremos univariados.
Las estadísticas de una distribución univariada se resumen en términos de cinco cantidades:
- Mínimo/máximo (bigotes)
- Primer y tercer cuantil (caja)
- Mediana (línea media de la caja)
- IQR = Q3 - Q1

Generalmente la regla de decisión:

- +/- 1.5*IQR Outliers moderados
- +/- 3 *IQR Outliers severos

![Box-Plot](Images/analisis-box-plot.png)

#### **Metodos Multivariados**

##### **Distancia de Mahalanobis**

Es una medida de distancia entre el punto y un conjunto de observaciones con media y una matriz de covarianza S.

![Mahalanobis](Images/mahalanobis.png)

![Mahalanobis](Images/mahalanobis-2.png)

##### **Local Outlier Factor (LOF)**

El LOF de una observación es una medida de su desviación de sus vecinos.

Es un método basado en densidad que utiliza la búsqueda de vecinos más cercanos.

- Se compara la densidad de cualquier punto de datos con la densidad de sus vecinos
- Parámetro k (cantidad de vecinos) y métrica de distancia

El método calcula los scores para cada punto, se debe definir un umbral de corte (depende del dominio)

- Si el score del punto X es 5, significa que la densidad promedio de los vecinos de X es 5 veces mayor que su densidad local

![LOF](Images/LOF.png)

##### **Isolation Forest**

Es un método de detección de outliers basado en árboles de decisión.

Tomar una muestra de los datos y construir un árbol de aislamiento:

1. Seleccionar aleatoriamente n características .
2. Dividir los puntos de datos seleccionando aleatoriamente un valor entre el mínimo y el máximo de las características seleccionadas.

La partición de observaciones se repite recursivamente hasta que todas las observaciones estén aisladas.

![Isolation Forest](Images/isolation-forest.png)

## **Arboles**

### **ID3 (Iterative Dichotomiser 3)**

Es un algoritmo de aprendizaje automático que genera un árbol de decisión a partir de un conjunto de datos. Fue creado por Ross Quinlan. SUs componenetes son:

- Nodo raiz que esta en la parte superior del arbol
- Nodos terminales que son nodos que no tienen hijos, o sea termina el flujo de decision y contiene la decision final/respueta
- Los nodos intermedios son nodos que tienen hijos y representan una decision con respecto a una variable
- Las lineas que conectan los nodos se llaman ramas y representan el flujo de decision

#### **ID3-Entropia**

La entropia es una medida de la incertidumbre de los datos. Para calcular la entropía de n clases se utiliza la fórmula: 

![Entropia](Images/entropia-id3.png)

Dónde:

- **S**: es una lista de valores posibles.
- **Pi:** es la probabilidad de los valores.
- **i:** Cada uno de los valores.

Importante

- Para una muestra homogénea la entropía es igual a 0
- La máxima entropía viene dada por Log2(n), n son los posibles valores de salida.  Si n =2 (TRUE o FALSE) entonces, la máxima entropía es 1 (máxima incertidumbre)
- Se usa para calcular la ganancia de la informacion

#### **ID3-Ganancia de Informacion**

La ganancia de información se aplica para cuantificar qué característica, de un conjunto de datos dados, proporciona la máxima información sobre la clasificación. ( Opuesto a entropia)

![Ganancia de Informacion](Images/ganancia-informacion.png)

Dónde:
- **S:** es una lista de valores posibles para un atributo dado: A
- **A:** Uno de los atributos, en la lista de ejemplo.
- **V(A)** : Conjunto de valores que A puede tomar
- **Sv/S** = Probabilidad de un valor, para el atributo A.
- **Entropía (Sv)**, entropía calculada para el valor “v” de A.

**TODO agrgar ejemplo**

### **Impureza de Gini**

La impureza de Gini es una medida de la probabilidad de que un elemento sea mal clasificado al elegir aleatoriamente un elemento de la misma clase. La impureza de Gini se calcula sumando la probabilidad de cada elemento de la clase elegida al cuadrado.

Algunas implementaciones de árboles de decisión utilizan la impureza de Gini en lugar de la ganancia de información, ya que es más fácil de calcular

![Gini](Images/gini.png)

**TODO agrgar ejemplo**

### **C4.5**

Es un algoritmo de aprendizaje automático que genera un árbol de decisión a partir de un conjunto de datos. Las mejoras que tiene con respecto a ID3 es:

- Campos numericos, rangos continuos
- Datos Faltantes
- Poda

#### **C4.5-Campos numericos, rangos continuos**

Si un atributo A, tiene un rango continuo de valores. El algoritmo puede, dinámicamente crear un campo Booleano tal que si A < C Ac = TRUE, sino Ac = FALSE.

Esto se logra calculando la ganancia de información para cada valor de C y seleccionando el valor que maximiza la ganancia de información.

#### **C4.5-Datos Faltantes**

Manejo de los datos de formación con valores de atributos faltantes - C4.5 permite valores de los atributos para ser marcado como “?” para faltantes. Los valores faltantes de los atributos simplemente no se usan en los cálculos de la ganancia y la entropía.

#### **C4.5-Poda**

El método de poda del árbol consiste en:

- generar el árbol tal y como hemos visto
- A continuación, analizar recursivamente, y desde las hojas, qué preguntas (nodos interiores) se pueden eliminar sin que se incremente el error de clasificación con el conjunto de test.

Pasos:

- Se elimina un nodo interior cuyos sucesores son todos nodos hoja.
- Se vuelve a calcular el error que se comete con este nuevo árbol sobre el conjunto de test.
- Si este error es menor que el error anterior, entonces se elimina el nodo y todos sus sucesores(hojas)
- Se repite.

### **Hiperparametros Arbol**

- **max_depth:** Profundidad máxima del árbol. Si es None, los nodos se expanden hasta que todas las hojas sean puras o hasta que todas las hojas contengan menos de min_samples_split muestras.
- **min_samples_split:** El número mínimo de muestras necesarias para dividir un nodo interno.
- **min_samples_leaf:** El número mínimo de muestras necesarias para estar en un nodo hoja. Un split válido requiere que al menos una de las salidas de la izquierda y la derecha contenga al menos min_samples_leaf muestras.
- **max_features:** La cantidad de características a considerar al buscar la mejor división.
- **max_leaf_nodes:** El crecimiento del árbol se detiene cuando el número de nodos hoja es mayor o igual a este valor.
- **ccp_alpha:** Parámetro de complejidad utilizado para la poda de costo-complejidad. Mayor ccp_alpha implica una mayor poda. (Solo disponible para la implementación de sklearn)
- **criterion:** Función para medir la calidad de una división. Los criterios soportados son "gini" para la impureza de Gini y "entropy" para la ganancia de información.

### **Random Forest**

Es un algoritmo de aprendizaje automático que genera un conjunto de árboles de decisión a partir de un conjunto de datos. Cada árbol de decisión del conjunto se genera a partir de un subconjunto de datos seleccionados al azar y con reemplazo. El algoritmo combina los resultados de cada árbol para encontrar la predicción final.

Utiliza la técnica de **Bootstrap aggregating**:

1. Dado un conjunto de entrenamiento **D**de tamaño **n**, se generan **m** nuevos conjuntos de tamaño **n** de forma aleatoria. Por lo general **n** es aproximadamente $`\frac{2}{3}`$ de **n**
2. Attribute bagging (o random subspace): Luego para cada una de las **m** tablas, escogemos sólo algunas columnas (raíz cuadrada del número de atributos) de forma aleatoria también.
3. Ahora tenemos **m** tablas reducidas en atributos y para cada una de ellas entrenamos un árbol:
    - Para cada árbol calculamos su matriz de confusión y calculamos la tasa de error de cada uno: (FP + FN) / TOTAL = **Tasa de error.**
    - Nos quedamos con el mejor árbol de los **m**, y repetimos el proceso **K** veces.
    - Al final vamos a tener **K**  árboles.
4. Una vez finalizado clasificamos una observación por votación de los **K**  árboles.

## **K-Nearest Neighbors (KNN)**

Es un algoritmo de aprendizaje automático que clasifica un punto de datos basándose en la clase de sus vecinos más cercanos. Es un algoritmo de aprendizaje supervisado.

Los hipermarametros son:

- k: cantidad de vecinos
- El método de distancia: euclidiana, manhattan, etc
- Peso: Los vecinos pueden estar ponderados, siendo que los más cercanos tengan más peso en la votación y los más lejanos menos.

Es sensible a conjuntos de datos no balanceados y outliers

### **KNN Hiperparametros**

La implementación de scikit learn nos permite setear los siguientes parámetros:

**n_neighbors**: número de vecinos a considerar, por defecto este valor esta seteado en 5.<br>
**algorithm**:algoritmo utilizado para calcular los vecinos más cercanos<br>
* ball_tree utilizará [BallTree](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.BallTree.html#sklearn.neighbors.BallTree)
* kd_tree : utilizará [KDTree](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KDTree.html)
* brute : utilizará una búsqueda de fuerza bruta.

**metric**:  métrica a utilizar para el cálculo de la distancia. El valor predeterminado es "minkowski".
Las métricas válidas dependerán del algorítmo que utilicemos.<br>
* eculidea
* manhattan
* city block
* ...

**weights**:función de peso utilizada en la predicción<br>

* uniform : pesos uniformes, todos los puntos en cada "vecindario" pesan lo mismo.  
*  distance : el peso de cada punto (vecino) se asigna en forma inversa a su distancia.

Esto son las metricas aceptados por los algoritmos de scikit learn:

```python
algoritmo: ball_tree
['euclidean', 'l2', 'minkowski', 'p', 'manhattan', 'cityblock', 'l1', 'chebyshev', 'infinity', 'seuclidean', 'mahalanobis', 'wminkowski', 'hamming', 'canberra', 'braycurtis', 'matching', 'jaccard', 'dice', 'kulsinski', 'rogerstanimoto', 'russellrao', 'sokalmichener', 'sokalsneath', 'haversine', 'pyfunc']

algoritmo: kd_tree
['euclidean', 'l2', 'minkowski', 'p', 'manhattan', 'cityblock', 'l1', 'chebyshev', 'infinity']

algoritmo: brute
['cityblock', 'cosine', 'euclidean', 'haversine', 'l2', 'l1', 'manhattan', 'precomputed', 'nan_euclidean', 'braycurtis', 'canberra', 'chebyshev', 'correlation', 'cosine', 'dice', 'hamming', 'jaccard', 'kulsinski', 'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto', 'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath', 'sqeuclidean', 'yule', 'wminkowski']

```

### **KNN Regresion**

La lógica de regresión KNN es muy similar a lo que se explicó anteriormente para clasificación.

El algoritmo KNN calculará la regresión para el conjunto de datos y luego tomará la cantidad K de vecinos, verificará los resultados de esos vecinos y promediará los resultados, generando una estimación.

Dada una nueva instancia, devolver el promedio(ponderado) de los valores de sus vecinos

## **Support Vector Machine (SVM)**

Las máquinas de soporte vectorial (SVM, del inglés support vector machines) son una técnica de aprendizaje automático supervisado. Si bien originalmente fueron diseñadas para resolver problemas de clasificación binaria, en la actualidad se aplican para resolver problemas más complejos como los de regresión, agrupamiento y multiclasificación.

Entre los campos de aplicación más difundidos podemos mencionar los siguientes:

* visión artificial
* reconocimiento de caracteres
* procesamiento de lenguaje natural
* análisis de series temporales

### **Vectores de Soporte**

![Vectores de Soporte](Images/vectoresDeSoporte.png)

* Las máquinas de vectores de soporte también dibujan un margen alrededor del límite de decisión(un margen nos da más "confianza" en nuestras predicciones).
La posición del margen se define utilizando los vectores que están más cerca del límite de decisión: los vectores que se encuentran en la parte superior del margen son los vectores de soporte.

* Las máquinas de vectores de soporte permiten cierta clasificación errónea durante el proceso de aprendizaje. Para que puedan hacer un mejor trabajo al clasificar la mayoría de los vectores en el conjunto de prueba.

### **Kerneles SVM**

* Las SVM pueden definir un límite de decisión lineal o no lineal mediante el uso de funciones kernel. El uso de kernels abren la posibilidad de abordar problemas más complejos.

* Los datos son mapeados por medio de la transformación kernel o núcleo, a un  espacio de dimensión mayor que el original en el cual se logra una mejor separación entre las clases. 

* Las SVM en realidad no calculan la transformación de cada observación en el espacio aumentado, en su lugar calculan el producto interno de las observaciones en el espacio aumentado que, computacionalmente, es mucho más barato.

#### **SVM con kernel LINEAL**

Hiperparametros:

* C : Parámetro de regularización, debe ser estrictamente positiva.

La fuerza de la regularización es inversamente proporcional a C
- un valor pequeño significa que el margen se calcula utilizando muchas o todas las observaciones alrededor de la línea de separación (más regularización).
- un valor grande significa que el margen se calcula sobre observaciones cercanas a la línea de separación (menos regularización).

#### **SVM con kernel POLINOMICO**

- El Kernel Polinómico de SVM sistemáticamente incrementa las dimensiones seteando d, desde 1 (o el valor base de las observaciones) hasta el valor del parámetro. EG: Con d=4, repite el proceso agregando una dimensión adicional. Lo mismo para cualquier d=n.
- Calcula en cada caso las relaciones entre las observaciones para encontrar un hiper-plano que parta el conjunto en 2. 

Hiperparametros:

* C : Parámetro de regularización.

* degree: Grado de la función kernel polinomial ('poly'). Ignorado por todos los demás núcleos

* gamma: define cuánta influencia tiene un solo ejemplo de entrenamiento. Cuanto más grande es, más cerca deben estar otros ejemplos para verse afectados.

* coef0 : se corresponde con el parámetro r de la ecucacion del kernel
K(a, b) =  (a . b + r) ^ d (d es el grado del polinomio).

La elección adecuada de C y gamma es fundamental para el rendimiento de la SVM. Se recomienda usar GridSearchCV con C y gamma espaciado exponencialmente para elegir buenos valores.

#### **SVM con kernel RADIAL**

El SVM Radial soporta Support Vector Classifiers en infinitas dimensiones

Hiperparametros:

* C : Parámetro de regularización.

* gamma:define cuánta influencia tiene un solo ejemplo de entrenamiento. Cuanto más grande es, más cerca deben estar otros ejemplos para verse afectados

La elección adecuada de C y gamma es fundamental para el rendimiento de la SVM. Se recomienda usar GridSearchCV con C y gamma espaciado exponencialmente para elegir buenos valores.

## **Ensambles**

Los ensambles son métodos que combinan varios algoritmos de aprendizaje automático para crear modelos más potentes. Los métodos de ensamblaje funcionan mejor cuando los modelos individuales son lo más independientes posible. Los métodos de ensamblaje se pueden dividir en dos grupos principales: métodos secuenciales y métodos paralelos.

### **Errores**

$`ErrTotal(x)=Bias^2+Varianza+Error Irreducible`$

![Errores](Images/errorEnsambles.png)

#### **Error irreducible**

No se puede reducir, independientemente de qué algoritmo se usa. También se le conoce como ruido y, por lo general, proviene por factores como variables desconocidas que influyen en el mapeo de las variables de entrada a la variable de salida, un conjunto de características incompleto o un problema mal enmarcado.

#### **Bias (sesgo)**

El error debido al Bias de un modelo es simplemente la diferencia entre el valor esperado del estimador (es decir, la predicción media del modelo) y el valor real. 

Un bias muy alto significa un modelo underfitteado → produce un error alto en todas las muestras: entrenamiento, validación y test.

![Bias](Images/bias.png)

#### **Varianza**

Es cuánto varía la predicción según los datos que utilicemos para el entrenamiento.

- Un modelo con varianza baja indica que cambiar los datos de entrenamiento produce cambios pequeños en la estimación.
- Un modelo con varianza alta quiere decir que pequeños cambios en el dataset conlleva a grandes cambios en el output (overfitting).

![Varianza](Images/varianza.png)

#### **Bias vs Varianza**

El bias frente a la varianza se refiere a la precisión frente a la consistencia de los modelos entrenados por su algoritmo.

![Bias vs Varianza](Images/biasVsVarianza.png)

### **Bagging**

Consiste en construir nuevos conjuntos de entrenamiento usando bootstrap (muestras aleatorias con reemplazo, lo que quiere decir que pueden haber datos que se repitan) para entrenar distintos modelos, y posteriormente combinarlos.

**-Pasos:**

1. Se divide el conjunto de entrenamiento en distintos subconjuntos, obteniéndose como resultado distintas muestras aleatorias.
   1. Las muestras son uniformes (misma cantidad de invididuos).
   2. Las muestras son con reemplazo (individuos pueden repetirse en el mismo conjunto de datos).
2. Se entrena un modelo con cada subconjunto.
3. Se construye un único modelo predictivo a partir de los anteriores.

**-Características:**

1. Disminuye la varianza en el modelo final.
2. Resulta ser efectivo en conjuntos de datos con varianza alta.
3. Puede reducir el overfitting.
4. Puede reducir el ruido de los outliers.
5. Puede mejorar levemente con el voto ponderado (no es que lo usa siempre).
6. Si pocos atributos son predictores fuertes, todos los árboles se van a parecer entre sí:
    Esos atributos terminarán cerca de la raíz, para todos los conjuntos generados con 
    bootstrap

Los random forest son una forma de bagging donde en cada nodo se eligen $m$ atributos al azar, en comparación con el bagging tradicional que toma todos. 

Se producen muchos árboles paralelamente (de forma aislada) sin podar.

### **Boosting**

Boosting es un método de ensamblaje secuencial que consiste en entrenar modelos de forma iterativa, donde cada modelo subsiguiente se enfoca en los casos que fueron mal clasificados por los modelos anteriores.

Pasos:

1. Comenzar con un modelo (simple) entrenado sobre todos los datos, al que llamaremos: $H_0$
2. En cada iteración $i$, entrenar $H_i$ dando mayor importancia a los datos mal clasificados por las iteraciones anteriores
3. Terminar al conseguir cierto cubrimiento, o luego de un número de iteraciones
4. Clasificar nuevas instancias usando una votación ponderada de todos los modelos construidos

Ventajas:

- Reduce el sesgo pero también la varianza para evitar el sobre-entrenamiento usando parámetros de regularización

Desventajas:

- Puede sobreajustar (los árboles overfittean bastante)

Modelos de boosting:

- AdaBoost
- Gradient Boosting
- XGBoost

#### **AdaBoost**

AdaBoost combina muchos "weak learners" para hacer clasificaciones. Se los denomina STUMPS (sólo un nodo y dos hojas) es decir que usan un único feature para decidir.

Cada STUMP tiene un peso en la clasificación final, que se calcula mediante una ponderación llamada ***Amount of say*** y para calcularla necesitamos el **Error Total** del tocón***.*** Si el ***amount of say*** es muy bajo (el nodo anterior no tiene mucho que decir), los pesos serán multiplicados por 1, o un número cercano a 1 y quedarán casi igual. Pero si el nodo anterior tiene mucho que decir los pesos serán disminuidos proporcionalmente. 

El peso depende de cuan bien clasificó las muestras (es una función del error total del stump). El que mejor clasificó tendrá mas peso.

Los STUMP se construyen teniendo en cuenta los errores del STUMP anterior. Por esto el orden es importante: los errores que comete un clasificador influyen en la construcción del siguiente.

La gran desventaja de este algoritmo es que no se puede paralelizar el modelo ya que cada predictor solo se puede entrenar después de haber entrenado y evaluado el anterior.

#### **Gradient Boosting**

>Gradient Boost puede usarse para problemas de regresión o de clasificación.

Gradient Boost comienza con un nodo hoja en lugar de un stump. Luego construye árboles pero de más de un nivel y todos aportan lo mismo a la clasificación final (no tienen pesos). Se construyen tantos árboles como se solicite o hasta que los árboles adicionales no logren mejorar el ajuste.

Permite elegir una función de costo L, por ejemplo, error cuadrático medio si es un problema de regresión, o entropía cruzada si es un problema de clasificación, y cada árbol que construye lo hace de manera que la función de costo se minimice (técnica de optimización que se denomina descenso por gradiente).

En cada iteración construimos un árbol nuevo que aprenda a predecir el residuo (error) entre las observaciones y los valores predichos por el árbol anterior.

Finalmente para estimar un nuevo valor, cuando el método termina de entrenar, procedemos como antes, usando el valor inicial y sumando los residuos ponderados (por un valor constante learning rate) de cada árbol.

Parámetros específicos del árbol: afectan a cada árbol individual del modelo.

- Criterio de elección de atributos en cada nodo (ganancia de la información o gini)
- Criterio de parada (ej: máxima profundidad generalmente se usa 5 o 6)
- Estrategia de poda

Parámetros de boosting: estos afectan la operación de boosting en el modelo.

- **learning_rate** (por defecto=0.1): la tasa de aprendizaje reduce la contribución de cada árbol → maneja el overfitting. Hay una compensación entre learning_rate y la cantidad de estimadores. Es un valor entre 0 y 1. Tendrá una varianza baja**.**
- **n_estimators** (por defecto=100): cantidad de estimadores que se van a utilizar.

#### **XGBoost**

>XG Boost puede usarse para problemas de regresión o de clasificación.

XGBoost es una forma más regularizada de Gradient Boosting . XGBoost utiliza la regularización avanzada (L1 y L2), que mejora las capacidades de generalización del modelo.

Ofrece un alto rendimiento en comparación con Gradient Boosting. Su entrenamiento es muy rápido y se puede paralelizar entre clústers. Puede trabajar con datos faltantes.

Pasos:

1. Hacer una predicción inicial. Esta predicción puede ser cualquier valor.
2. Construir un árbol para los residuos. Este árbol es diferente a los usados por Gradient Boost. Primero se crea un nodo hoja y se ponen allí **todos** los residuos.
3. Calcular el ***Similarity Score***, para los residuos: $`Similarity Score=\frac{(\sum{residuos)^2}}{\#residuos + \lambda}`$
4. Tenemos que ver cuál será el siguiente nodo. Para ello vamos a calcular la **ganancia total** (Gain), según escojamos una opción u otra para partir el árbol (qué umbral tomemos). Nos quedamos con el umbral que de la mayor información ganada.
5. Repetimos el anterior hasta alcanzar la profundidad del árbol estipulada.
6. Poda:
    - Elegimos un número al azar → este número se llama gamma ( **𝜸** )
    - Calculamos la diferencia entre el **Gain** del nodo más bajo y **gamma**
    - Si la diferencia es < 0 → removemos el nodo
    - Sino, el nodo se queda y se terminó la poda
7. Volvemos a calcular el árbol (repite paso 3), solo que esta vez usamos lambda 𝛌 igual a 1 al calcular el Similarity Score y el Gain.
8. Podamos (paso quinto nuevamente).
9. Con estos nuevos residuos, construimos un nuevo árbol.
    
    Repetimos todo, desde el paso 2.
    
    Con el nuevo árbol, calculamos la salida de cada elemento y luego los residuos.
    
    Construimos otro árbol.
    
    Seguimos hasta que los residuos son prácticamente cero o bien alcanzamos el número máximo de árboles predefinido.
    

Hiper-parámetros:

- learning_rate: tasa de aprendizaje (por defecto es 0.3)
- max_depth: máxima profundidad de cada árbol (por defecto **XGBoost**
 trabaja con 6 niveles)
- subsample: porcentaje de muestras usadas para cada árbol (valor muy bajo, posible underfitting)
- colsample_bytree: porcentaje de features usadas para cada árbol (valores muy alto, posible overfitting)
- n_estimators: cantidad de árboles a construir.
- objective: función de error a utilizar (algunas: reg:linear para regresión, reg:logistic o binary:logistic para clasificación)

Parámetros de regularización: 

- **gamma**: umbral para hacer split basado en la reducción de error de hacer el nuevo split. Por defecto gamma es igual a 0. Cuanto más alto más conservador es el algoritmo.
- **alpha**: regularización para los pesos de las hojas. Un valor más alto genera una mayor regularización.
- **lambda**: similar alpha pero para la sintonía fina. Con 𝛌 reducimos el error por varianza.
    - Cuando **𝛌 > 0** es más probable tener que podar un árbol, ya que los **Gain** calculados son menores. Con lambda > 0, los Similarity Scores son mucho más chicos.
    - Aún eligiendo **𝜸 = 0** podemos tener que podar nodos, ya que la ganancia (**Gain**) puede ser negativa.
    - **𝛌=1** previene el sobreentrenamiento o sobreajuste del modelo en el conjunto de entrenamiento.

## **Ensambles Hibridos**

Combinan clasificadores de distinto tipo. La combinación de clasificadores permite generar clasificadores más precisos a cambio de menor comprensión de los mismos.

Ventajas:

- Logran mejores predicciones
- Buen trade-off sesgo varianza

Desventajas:

- Se pierde interpretabilidad → son como “caja negra”
- Complejidad computacional mayor

### **Voting**

Construir N modelos utilizando los mismos datos y luego tomar la predicción mayoritaria o ponderada. 

El clasificador de votaciones tiene dos hiperparámetros básicos: *estimadores* y *votaciones*.

El hiperparámetro *estimators* crea una lista los clasificadores y les asigna nombres.

El hiperparámetro de *votación* se establece en "hard" o "soft".

- hard: el clasificador de votación usa las etiquetas de clase predichas para la votación por regla de la mayoría.
- soft: utilizará un enfoque ponderado para tomar su decisión (predice la etiqueta de clase en función del argmax de las sumas de las probabilidades predichas)

![Voting](Images/voting.png)

### **Stacking**

Entrena diferentes modelos (modelos base) y un modelo más, que decide, dada una instancia nueva, qué modelo usar. Concepto de meta-aprendizaje para reemplazar el mecanismo de voto. Pueden apilarse tantas capas de modelos como se desee.

![Stacking](Images/stacking.png)

### **Cascading**

Enfoque en el que se pasa sucesivamente los datos de un modelo a otro. A diferencia de Stacking cada “capa” tiene un sólo modelo. El input de cada modelo son las instancias predichas con poca certeza por el modelo anterior. Suele utilizarse cuando se necesita una alta certeza en la predicción.

Entrenamos N modelos distintos utilizando nuestro set de entrenamiento. Cada modelo lo entrenamos sobre las instancias predichas con baja certeza del modelo anterior. Suele hacerse de forma tal de empezar por modelos simples y a medida que se entrenan nuevos los mismos sean de mayor complejidad.

## **Reducción de Dimensionalidad**

Visualización de datos en una dimensión menor (2D o 3D), preservando características importantes de estos como distancias, correlaciones, etc.

Usos

- Visualización de datos para entender su distribución o detectar patrones invisibles a simple vista
- Reducción del ruido
- Aceleración de los tiempos de entrenamiento de un modelo
- Compresión de la información
- Presentación de resultados a interesados (quienes no siempre conocen de ciencia de datos)

### **PCA**

Usos:

- Identificar si hay agrupamiento de datos en el espacio de entrada
- Identificar **correlaciones** o bien entender cuán dispersos están los datos y sobre todo, sobre qué ejes o variables. PCA preserva las correlaciones!
- Es útil especialmente cuando no podemos representar el espacio de entrada sobre un eje cartesiano
- Las nuevas variables (componentes) se obtiene a partir de combinaciones lineales de las variables originales

> Antes de ejecutar un algoritmo de PCA se deben normalizar los atributos (por ejemplo con min-max o standar scaler)

Pasos:

1. Calculamos el valor promedio para cada feature y calculamos el centroide
2. Centramos el conjunto de datos de manera que el centroide quede en el origen
3. Trazamos una recta aleatoria que pase por el origen y la rotamos hasta que ajuste bien a los datos
   - En la línea aleatoria proyectamos cada punto y buscamos minimizar la distancia de la proyección a la observación o bien maximizar la distancia de la proyección al origen de coordenadas lo cual resulta equivalente por pitágoras. 
   - PCA maximiza la suma de distancias cuadradas llamadas autovalores.
4. Calculamos los autovectores
5. Calculamos la siguiente componente

**¿Con cuántas PC nos quedamos?**

Para determinar esto debemos calcular la varianza explicada, es decir una magnitud que nos diga qué tanta variabilidad de los datos representa cada componente. Nos quedamos con aquellas que sumadas me representen un buen porcentaje.

La variación centrada en el origen podemos calcularla como:

```math
Variacion PCi=\frac{autovalor PCi}{n-1}
```




### **MDS - Multi-Dimensional Scaling**

La idea es preservar la distancia entre los puntos y ubicar los puntos en un espacio de menor dimensión tal que las distancias se parezcan lo más posible.

Fortalezas:

- Soporta varios tipos de distancias
- Permite transformaciones no lineales

Debilidades:

- Optimización iterativa con mínimos locales
- Difícil determinar qué distancia a usar es la mejor

### **t-SNE**

El objetivo es conservar los clusters

Fortalezas:

- De lo mejor para visualizar datos
- Conserva estructuras no lineales globales y locales

Debilidades:

- Es estocástico (es no determinista) (un modelo es determinístico cuando se tiene certeza de los valores de los parámetros, el modelo es estocástico cuando los parámetros usados para caracterizar el modelo son variables aleatorias que tienen unos comportamientos estimados pero no se conoce con certidumbre previamente cuál será el valor que tomen)
- Escala mucho en tiempo con dimensiones y puntos: mayor costo computacional
- No se puede usar para nuevos puntos

### **ISOMAP**

Permite aproximar la forma de los datos y calcular la distancia Geodésica.

La distancia geodésica conserva la forma de los datos, uniendo dos puntos con una línea de mínima longitud pero contenida en la superficie. Pero no sé a priori si los datos siguen una forma particular o no. Y muchos menos sé qué forma podría ser esa →uso isomap.

Debilidades:

- Es un algoritmo lento, a partir de 1000 observaciones comienza a notarse la lentitud
- Puede crear una proyección errónea si $K$ es demasiado grande con respecto a la estructura de la variedad (variedad es una porción del espacio, de dimensión **n**
 que se parece a ℝn, en un espacio de dimensión mayor) o si hay ruido en los datos
- Si $K$ es demasiado pequeño, el gráfico de vecindad puede volverse demasiado escaso para aproximar las trayectorias geodésicas con precisión

## **Redes Neuronales**

Se componen de un input, pesos, suma de los input*pesos (suma ponderada), una función de activación y un output. Funcionan para regresión y clasificación.

Las redes neuronales son sensibles a cambios de escala → ESCALAR los datos antes

### **Perceptron Simple**

Es un algoritmo de clasificación binaria que se utiliza para predecir la etiqueta de una clase. Es un tipo de clasificador lineal, es decir, el algoritmo de clasificación que hace sus predicciones sobre la base de una función lineal de sus características.

El perceptrón es un algoritmo de clasificación binaria, lo que significa que solo puede predecir entre dos clases. Por lo tanto, solo puede resolver problemas que sean linealmente separables, es decir, cuando una línea recta puede separar las dos clases de datos.

 Puedo representar una compuerta AND (también OR/NAND/NOR) con un perceptrón simple

### **Preceptron Multicapa**

Se compone de una capa de entrada, una o más capas ocultas y una capa de salida. Cada capa está compuesta por neuronas. Cada neurona de una capa está conectada con todas las neuronas de la capa siguiente. Las neuronas de la capa de entrada reciben los datos y las de la capa de salida devuelven la respuesta del modelo.

#### **Backpropagation**

Es un algoritmo de entrenamiento para redes neuronales artificiales de múltiples capas. El método calcula el gradiente de una función de pérdida con respecto a todos los pesos de la red neuronal. El gradiente se calcula a partir de la capa final hacia atrás hasta la primera capa oculta. El gradiente resultante se usa luego para actualizar los pesos, reduciendo así la pérdida de la red neuronal.

#### **Redes SOM(Self Organizing Maps)**

Son redes neuronales no supervisadas que se utilizan para reducir la dimensionalidad de los datos. Se utilizan para visualizar datos de alta dimensión en un mapa de baja dimensión, generalmente un mapa bidimensional.

Sólo gana la neurona que tenga la menor distancia. Al suceder esto, se actualizan los pesos que estén en un radio determinado respecto de la ganadora, dándole más peso. Estos radios cada vez se hacen más chicos hasta que dejan de actualizarse y obtenemos clusters. ES UN ALGORITMO DE CLUSTERIZACIÓN no supervisado.

## Implementación de Redes Neuronales

¿Qué funciones de activación uso?

- Si mi problema es de regresión → sigmoidea, lineal, ReLu
- Si mi problema es de clasificación de clases excluyentes (es A o B no ambas) → sigmoidea
- Si mi problema es de clasificación de N clases simultáneas (puede ser un poco de A y de B) → softmax. Devuelve una probabilidad.

![Funciones de activacion](Images/implementacionRedesNeuronales.png)

### **Overfitting en Redes Neuronales**

#### **Metodos de regularización**
##### **L1 y L2**

Penalizan el valor de los pesos de la red. Esto evita que se le de más relevancia a una característica que a otra. Se le agrega un término en la función de costos proporcional a los pesos. Si es proporcional al módulo de los pesos se llama Regularización L1, si es proporcional al módulo cuadrado se le llama Regularización L2.

##### **Dropout**

Evita codependencias en las conexiones de la red. La idea es “apagar” activaciones aleatoriamente durante el entrenamiento. Esto hace que el buen funcionamiento de la red no dependa de unas pocas neuronas. Cuando entrena no lo hace siempre por los mismos caminos.

##### **Early stopping**

La idea es evitar el sobreajuste parando el entrenamiento antes de que el error del set de validación empieza aumentar. Este método busca entonces quedarse con los pesos en la instancia óptima.

##### **Data augmentation**

La idea es agregar datos usando los datos que se tienen y aplicarles transformaciones que los conviertan en nuevos datos, de manera que sean verosímiles. Es especialmente útil cuando se trabaja con imágenes. En general es difícil encontrar las transformaciones a aplicar.

#### **Optimizadores**

Un optimizador es una implementación concreta del algoritmo de backpropagation.

![Optimizadores](Images/optimizador.bin)

##### **SGD**

Backpropagation simple, sin ningún tipo de optimización

##### **Momentum**

A diferencia del backpropagation tradicional, en donde los pasos son regulares aquí son cada vez más rápidos. Toma la idea de la física.

El gradiente se utiliza para la aceleración y no para la velocidad.

El hiperparámetro momentum va entre 0 y 1, en donde 0 = mucha fricción, 1 = nada de fricción. Estabiliza la velocidad final.

##### **Nesterov**

Variante de Momentum, en vez de calcular el gradiente del error en el punto actual, lo calcula un poco más adelante (en la dirección del momento): θ + 𝛃m .

(Para implementar Nesterov, en Keras, tenemos que usar el optimizador SGD, pero con la opción nesterov en True)

##### **Adagrad**

Uno de los problemas de los otros métodos es que en N dimensiones, el error descenderá por la dimensión con la pendiente más empinada, que no necesariamente será que conduzca al mínimo global. Esto implica que hallará primero el mínimo local (más empinado) y luego irá lentamente hacia el mínimo global.

AdaGrad reduce el vector gradiente a lo largo de las dimensiones más empinadas. Esto hace que el descenso sea más lento en las dimensiones más empinadas y más rápido en las dimensiones más planas.

##### **RMSProp**

Soluciona el principal problema de AdaGrad al ir “olvidando” las pendientes anteriores, a medida que sigue avanzando. Es decir, solo acumula los gradientes de las iteraciones más recientes.

𝛽 es la tasa de decaimiento, y suele configurarse como 0.9. Es un nuevo hiperparámetro, pero suele funcionar bien con este valor predeterminado. (En keras rho es beta).


##### Adam (Adaptive moment estimation)
    
Combina las ideas de Momentum y RMSProp. Hace un seguimiento de una media de decaimiento exponencial de gradientes pasados y de gradientes cuadrados pasados.
    
Los parámetros “sombrero” tienen una finalidad para la primera parte para que el algoritmo no tarde tanto en arrancar.
    

##### AdaMax
    
Es una modificación de Adam. En general Adam da mejores resultados, pero depende del conjunto de datos.
    
Se elimina s sombrero
    
##### Nadam

Es Adam + Nesterov, asi que a menudo converge más rápido que Adam.

##### Adadelta
    
Es una variación de AdaGrad en la que en vez de calcular el escalado del factor de entrenamiento de cada dimensión, teniendo en cuenta el gradiente acumulado desde el principio de la ejecución, se restringe a una ventana de tamaño fijo de los últimos n gradientes. Similar a RMSProp que va olvidando los gradientes.


### Número de Capas

Para muchos problemas 1 capa oculta será suficiente. En teoría un perceptrón multicapa con una sola capa oculta puede modelizar funciones complejas. Tendrá que tener neuronas suficientes.

Pero si estamos ante problemas más complejos, las redes profundas tendrán mejor desempeño, ya que pueden modelizar mejor con menos neuronas totales.

El número de neuronas de la capa de entrada y de salida está determinado por el problema a resolver. Lo habitual es hacer una pirámide. Poniendo cada vez menos neuronas. Sin embargo últimamente se ha cuestionado esta técnica, ya que a veces poner la misma cantidad de neuronas en todas las capas da el mismo resultado o a veces mejor.

### Otros Hiperparámetros

- Learning rate o tasa de aprendizaje: es el más **importante**, indica qué tan rápido se va desciendo en la función de costo. Valores usuales: E-01 a E-04